Примеры
=======================================

В нижней правой части примеров подсвечиваются клавиши

.. tabs::

   .. tab:: Выделить прямоугольник

      .. image:: images/capture1.gif
         :width: 800
         :alt: Alternative text

   .. tab:: Исправить ошибку

      .. image:: images/capture2.gif
         :width: 800
         :alt: Alternative text

   .. tab:: Сменить изображение

      .. image:: images/capture3.gif
         :width: 800
         :alt: Alternative text

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Инструкция
