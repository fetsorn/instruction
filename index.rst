.. labelling documentation master file, created by
   sphinx-quickstart on Sat May 16 02:49:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
Разметка хлеба на Supervisely
=======================================

.. meta::
   :description lang=ru: Инструкция, разметка хлеба, Supervisely.

Будут даны кадры из видео, на каждом из которых запечатлены батоны хлеба.
Требуется обести прямоугольниками все батоны на выходе из печи и на конвейере.

.. image:: images/example-nolabels.png
   :width: 600
   :alt: Alternative text

.. image:: images/example-labels.png
   :width: 600
   :alt: Alternative text

Данные для разметки и инструментарий расположены на онлайн-платформе Supervisely. Всего требуется разметить примерно 300 кадров одинаковой сложности, которые будут разделены между несколькими людьми.

* **Supervisely**:
  :doc:`Как пользоваться сервисом <supervisely>`

* **Требования к разметке**:
  :doc:`Хлеб <bread>`


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Инструкция

   supervisely
   bread
   examples
